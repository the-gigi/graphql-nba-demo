const express = require('express')
const graphqlHTTP = require('express-graphql')
const app = express()
const {buildSchema} = require('graphql')
const _ = require('lodash/core')

schema = buildSchema(`
  type Player {
    id: ID
    name: String!
    championshipCount: Int!
    team: Team!
  }
  
  type Team {
    id: ID
    name: String!
    championshipCount: Int!
    players: [Player!]!
  }
  
  type Query {
    allPlayers(offset: Int = 0, limit: Int = -1): [Player!]!
  }
  
  type Mutation {
    createPlayer(name: String, championshipCount: Int, teamId: String): Player
  }`
)


let data = {
  'allPlayers': {
    '1': {
      'id': '1',
      'name': 'Stephen Curry',
      'championshipCount': 2,
      'teamId': '3'
    },
    '2': {
      'id': '2',
      'name': 'Michael Jordan',
      'championshipCount': 6,
      'teamId': '1'
    },
    '3': {
      'id': '3',
      'name': 'Scotty Pippen',
      'championshipCount': 6,
      'teamId': '1'
    },
    '4': {
      'id': '4',
      'name': 'Magic Johnson',
      'championshipCount': 5,
      'teamId': '2'
    },
    '5': {
      'id': '5',
      'name': 'Kobe Bryant',
      'championshipCount': 5,
      'teamId': '2'
    },
    '6': {
      'id': '6',
      'name': 'Kevin Durant',
      'championshipCount': 1,
      'teamId': '3'
    }
  },

  'allTeams': {
    '1': {
      'id': '1',
      'name': 'Chicago Bulls',
      'championshipCount': 6,
      'players': []
    },
    '2': {
      'id': '2',
      'name': 'Log Angeles Lakers',
      'championshipCount': 16,
      'players': []
    },
    '3': {
      'id': '3',
      'name': 'Golden State Warriors',
      'championshipCount': 5,
      'players': []
    }
  }
}

rootValue = {
  allPlayers: (args) => {
    var offset = args['offset']
    var limit = args['limit']
    var players = _.values(data['allPlayers'])
    var r = players.slice(offset)
    if (limit > -1) {
      r = r.slice(0, Math.min(limit, r.length))
    }
    _.forEach(r, (x) => {
      var team = data.allTeams[x.teamId]
      if (team.players.length === 0) {
        for (var i = 0; i < players.length; ++i) {
          if (players[i].teamId === x.teamId) {
            data.allTeams[x.teamId].players.push(players[i])
          }
        }
      }
      data.allPlayers[x.id].team = data.allTeams[x.teamId]

    })
    console.log(r)
    return r
  },
  createPlayer: (args) => {
    id = (_.values(data['allPlayers']).length + 1).toString()
    args['id'] = id
    args['team'] = data['allTeams'][args['teamId']]
    data['allPlayers'][id] = args
    return data['allPlayers'][id]
  },
  hello: () => {
    return 'Yeah, it works!'
  }
}


app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: rootValue,
  graphiql: true
}))

app.listen(4000)

module.exports = app